import { RedisPubSub } from 'graphql-redis-subscriptions';
const { gql, withFilter } = require('apollo-server');
const { makeExecutableSchema } = require('graphql-tools');
const CHAT_SENT_TOPIC = 'newChat';
const redis = require("redis");
const client = redis.createClient({ port: process.env.REDDISPORT, host: process.env.REDDISHOST, password: process.env.REDDISPW });
const pubsub = new RedisPubSub({
  publisher: client,
  subscriber: client
});

const typeDefs = gql`

  type User {
    First_Name: String
    Last_Name: String
    id: Int
    user_avatar: String
  }


  type Chat {
    id: Int
    username: String
    message: String
  }

 type Query {
    Auth: User
  }

  type Subscription {
        chatSent: Chat    
  }

`;


const resolvers = {
  Query: {
    Auth: async (parents: any, args: any) => {
      const status = { dog: "cow" }
      return status
    }
  },

  Subscription: {
    chatSent: {
      subscribe: () => pubsub.asyncIterator(CHAT_SENT_TOPIC)
    },
  },
}



const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

export default schema;